# MonopolyCube ambientato nel palazzo delle scienze #

## Progetto creato come progetto finale per il corso di PR1 dell'anno 2016 dell'Università di Cagliari. ##

Implementa il gioco del monopoli ambientato nel palazzo delle scienze (solo testuale, GUI non era richiesta).

Per maggiori informazioni è disponibile un pdf con le specifiche del progetto nella cartella compressa FP1.5.zip

## Valutazione finale: 25/20 (vedi pdf sopra citato per le regole di valutazione) ##